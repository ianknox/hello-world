####Installation:
* `git clone`
* `pip install -r requirements.txt`

####Running locally:
* `pip install gunicorn`
* `gunicorn app:api`
* ` curl http://localhost:8000/example` to test

####Testing:
* `pip install nose`
* `nosetests -v app.py`

####Lambda Deployment:
* Edit ~/.aws/credentials and add valid credentials as specified by AWS: https://goo.gl/p4vvvX 
* Edit `zappa_settings.json` to reflect the correct `s3_bucket` and `profile_name` and env name (default is `dev`, but you can add as many targets as you like)

* `zappa deploy <env name, I.E. 'dev'>`
* `curl <url from output>/example` to test
