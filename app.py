import falcon
import falcon.testing as testing
import json


class ExampleResource:
    """
    This class handles all calls to a specified endpoint.  In this case "example"
    """

    def __init__(self):
        # load the config
        self.config = json.loads(open('config/config.json', 'r').read())

    def on_get(self, req, resp):
        """
        Handles GET requests
        """
        resp.body = json.dumps({'message': 'Hello World'})


class TestExample(testing.TestBase):
    """
    Tests ExampleResource
    """

    def before(self):
        self.api.add_route('/example', ExampleResource())

    def test_example(self):
        self.assertEqual(self.simulate_request('/example', decode='utf-8'), u'{"message": "Hello World"}')
        self.assertEqual(self.srmock.status, falcon.HTTP_200)


api = falcon.API()
api.add_route('/example', ExampleResource())
